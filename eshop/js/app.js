var app = angular.module('EshopApp', ['ui.router', 'ngCookies']);

app.controller('globalController', ['$scope', 'Categories', '$timeout', 'CartService', 'Currency', '$state',
    function($scope, Categories, $timeout, CartService, Currency, $state){
    $scope.categories = [];
    $scope.catLoader = function() {
        Categories.getCategories().then(function (response) {
            $scope.categories = Categories.getCategoriesTree(response.data['categories']);
        });
        $timeout($scope.catLoader, 15000);
    };
    Currency.loadCurrencies().then(function(response){
        $scope.currencyRates = response.data.rates;
        Currency.setRates(response.data);
    });
    $scope.catLoader();
    $scope.myCart = CartService;
    $scope.currency = Currency.getCurrent();
    $scope.setCurrency = function(code){
        Currency.setCurrent(code);
        $scope.currency = code;
        //$state.reload();
    };

    $scope.cartChangeQuantity = function(productId, diff) {
        CartService.changeQuantity(productId, diff);
    };

    $scope.addToCart = function(productId, price){
        CartService.putItem(productId, 1, price);
    };

    $scope.cartRemoveItem = function(productId) {
        CartService.removeItem(productId);
    };

    $scope.cartClear = function() {
        CartService.removeAll();
    }

}]);

var currencyResolver = function(Currency){
    return Currency.loadCurrencies();
}

app.config(['$urlRouterProvider', '$stateProvider', function($urlRouterProvider, $stateProvider){
    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('main', {
            'url': '/',
            'templateUrl': 'templates/mainpage.html',
            'controller': 'mainPageController',
            'resolve': {
                resolvedProducts: function(Products){
                    return Products.getProducts()
                },
                currencies: currencyResolver
            }
        })
        .state('cart', {
            'url': '/cart',
            'templateUrl': 'templates/cart.html',
            'controller': 'cartController',
            'resolve': {
                currencies: currencyResolver,
                resolvedProducts: function(Products){
                    return Products.getProducts()
                }
            }
        })
        .state('category', {
            'url': '/category/:catId',
            'templateUrl': 'templates/category.html',
            'controller': 'categoryController',
            'resolve': {
                currencies: currencyResolver
            }
        })
        .state('product', {
            'url': '/product/:productId',
            'templateUrl': 'templates/productDetail.html',
            'controller': 'productDetailController',
            'resolve': {
                currencies: currencyResolver
            }
        })
}]);
