
angular.module('EshopApp').service('Backend', ['$http', '$timeout', function($http, $timeout){
   var self = this;

   self.data = {};

   self.makeRequest = function(url, params) {
      params = params || {};
      if(angular.isObject(params) && !angular.equals({}, params)) {
         url += '?' + jQuery.param(params);
      }
      return $http.get(url);
   };

   self.getCategories = function() {
      return self.makeRequest('data/categories.json');
   };

   self.getProducts = function() {
      return self.makeRequest('data/products.json');
   };
    self.loadCurrencies = function(){
        //return self.makeRequest('http://api.fixer.io/latest?base=USD');
        return self.makeRequest('data/currencies.json');
    }
}]);