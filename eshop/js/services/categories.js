
angular.module('EshopApp').service('Categories', ['Backend', function(Backend){
   var self = this;
   self.getCategories = Backend.getCategories;
   self.groupedByParents = function(catList) {
      var grouped = {};
      for(item in catList) {
         var category = catList[item];
         if(Object.keys(grouped).indexOf(category['parent'].toString()) == -1){
            grouped[category['parent'].toString()] = [];
         }
         grouped[category['parent'].toString()].push(category);
      }
      return grouped;
   };
   self.parseRecursive = function(grouped, parent_id) {
      var data = [];
      var tree = grouped[parent_id.toString()];
      if(!tree.length) return data;
      for(item in tree) {
         data[item] = tree[item];
         if(data[item]['id'].toString() in grouped){
            data[item]['nodes'] = self.parseRecursive(grouped, data[item]['id']);
         }
      }
      return data;
   };
   self.getCategoriesTree = function(catlist) {
       var grouped = self.groupedByParents(catlist);
       return self.parseRecursive(grouped, '0');
   };
   self.getChildCategories = function(catList, catIdParent){
       var catIds = [];
       var getChilds = function(catId) {
         for(item in catList) {
            var category = catList[item];
            if(category['parent'].toString() == catId.toString() && catIds.indexOf(category['id']) == -1){
               catIds.push(category['id'].toString());
               getChilds(category['id']);
            }
         }
       };
       getChilds(catIdParent);
       return catIds;
   };
   self.getCategoryById = function(catList, catId) {
      for(item in catList) {
         var category = catList[item];
         if(category['id'].toString() == catId.toString()){
            return category;
         }
      }
   }
}]);