
angular.module('EshopApp').service('Currency', ['Backend', '$cookies', function(Backend, $cookies) {
   var self = this;
   self.currencies = {
       'USD': {'sign': '$'},
       'EUR': {'sign': '\u20AC'},
       'GBP': {'sign': '\u00A3'},
       'RUB': {'sign': 'RUB'}
   };
   self.loadCurrencies =  Backend.loadCurrencies;
   self.setRates = function(data){
      self.rates = data.rates;
      self.rates[data.base] = 1;
   };
   self._current = $cookies.get('CurrentCurrency') || 'USD';
   self.setCurrent = function(code) {
       self._current = code;
       $cookies.put('CurrentCurrency', code);
   };
   self.getCurrent = function() {
       return self._current;
   };
}]);