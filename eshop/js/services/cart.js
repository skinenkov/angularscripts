
angular.module('EshopApp')
    .service('CartService', ['$cookies', '$state', function($cookies, $state){
       var self = this;
       self.cartItems = [];
       self.getItems = function() {
            self.cartItems = $cookies.getObject('cartItems') || self.cartItems;
            return self.cartItems;
       };
       self.getItems();
       self.isEmpty = function() {
           return self.cartItems.length > 0;
       };
       self.putItem = function(productId, quantity, price) {
           var done = false;
           self.getItems();
           for(ind in self.cartItems) {
               var item = self.cartItems[ind];
               if(item['id'].toString() == productId.toString()) {
                   item['quan'] = item['quan'] + +quantity;
                   done = true;
                   break;
               }
           }
           if(!done) {
               self.cartItems.push({'id': productId, 'quan': +quantity, 'price': price});
           }
           $cookies.remove('cartItems');
           $cookies.putObject('cartItems', self.cartItems);
       };
       self.getTotalCount = function() {
           var total = 0;
           for(ind in self.cartItems) {
               total += self.cartItems[ind]['quan'];
           }
           return total;
       };
       self.getTotalCost = function() {
           var total = 0;
           for(ind in self.cartItems) {
               total += self.cartItems[ind]['price'] * self.cartItems[ind]['quan'];
           }
           return total;
       };
       self.changeQuantity = function(productId, diff) {
           for(ind in self.cartItems) {
               var item = self.cartItems[ind];
               if(item['id'].toString() == productId.toString()) {
                   item['quan'] = item['quan'] + +diff;
                   if(item['quan'] <= 0) {
                       return self.removeItem(productId);
                   }
                   break;
               }
           }
           $cookies.remove('cartItems');
           $cookies.putObject('cartItems', self.cartItems);
           self.getItems();
       };
       self.removeItem = function(productId) {
           var Index = -1;
           for(ind in self.cartItems) {
               var item = self.cartItems[ind];
               if(item['id'].toString() == productId.toString()) {
                   Index = ind;
                   break;
               }
           }
           self.cartItems.splice(Index, 1);
           $cookies.remove('cartItems');
           $cookies.putObject('cartItems', self.cartItems);
           self.getItems();
       };
       self.removeAll = function() {
           $cookies.remove('cartItems');
           $cookies.putObject('cartItems', []);
       }
    }]);