
angular.module('EshopApp')
    .service('Products', ['Backend', 'objectsByKeyValueFilter', function(Backend, objectsByKeyValue){
        var self = this;
        self.getProducts = Backend.getProducts;
        self.getProductsByCategory = function(products, catIds) {
            return objectsByKeyValue(products, 'catid', catIds);
        };
        self.getRandomProduct = function(products) {
            var ind = parseInt((Math.random() * 1000) % products.length);
            return products[ind];
        };
        self.getRandomProducts = function(products, num) {
            var randomProducts = [];
            for(var i=0; i<num; i++){
                var j = 0;
                while(j < products.length) {
                    product = self.getRandomProduct(products);
                    if (randomProducts.indexOf(product) == -1) {
                        randomProducts.push(product);
                        break;
                    }
                    j++;
                }
            }
            return randomProducts;
        };
        self.getProductById = function(products, productId) {
            for(item in products){
                var product = products[item];
                if(product['id'].toString() == productId.toString()){
                    return product;
                }
            }
        }
    }]);