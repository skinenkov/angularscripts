
angular.module('EshopApp').filter('strCutTo', function() {
    return function(str, num) {
        var num = +num;
        if (str.length < num) return str;
        else {
            return str.substr(0, Math.abs(num - 3)) + '...';
        }
    }
})