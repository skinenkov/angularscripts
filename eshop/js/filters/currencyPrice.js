
angular.module('EshopApp').filter('currencyPrice', ['Currency', function(Currency){
   return function(value, curr, rates){
       sign = Currency.currencies[curr].sign;
       if(rates && (curr in rates) && rates[curr] != 1) {
           value = (value * rates[curr]).toFixed(2);
       } else {
           sign = '$';
       }
       return sign + ' ' + value;
   }
}]);