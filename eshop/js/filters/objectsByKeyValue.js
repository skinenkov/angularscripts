
angular.module('EshopApp').filter('objectsByKeyValue', function() {
    return function(objectsList, key, valuesArr){
        var returnList = [];
        for(var i=0; i<objectsList.length; i++){
            var item = objectsList[i];
            if(typeof(item) == 'object' && key in item && valuesArr.indexOf(item[key].toString()) != -1 && returnList.indexOf(item) == -1) {
                returnList.push(item);
            }
        }
        return returnList;
    }
});