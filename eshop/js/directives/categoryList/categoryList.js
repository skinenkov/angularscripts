angular.module('EshopApp')
    .directive('categoryList', function(){
       return {
           restrict: 'E',
           scope: {
               categories: '='
           },
           templateUrl: 'js/directives/categoryList/categoryList.html'
       }
    }).directive('subCategories', function() {
        return {
            restrict: 'E',
            scope: {
                'nodes': '='
            },
            templateUrl: 'js/directives/categoryList/subCategories.html'
        }
});