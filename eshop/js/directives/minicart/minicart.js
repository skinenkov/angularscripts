
angular.module('EshopApp').directive('miniCart', ['Products', function(Products){
    return {
        restrict: 'E',
        scope: {
            cartItems: '=',
            cartCost: '=',
            currencyRates: '=',
            currency: '='
        },
        templateUrl: 'js/directives/minicart/minicart.html'
    }
}]);