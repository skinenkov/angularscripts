
angular.module('EshopApp').directive('productsGrid', function() {
   return {
       restrict: 'E',
       scope: {
           'products': '=',
           'title': '@',
           'addToCart': '&',
           'currency': '=',
           'currencyRates': '='
       },
       templateUrl: 'js/directives/productsGrid/productsGrid.html'
   }
});