
angular.module('EshopApp').controller('productDetailController', ['$scope', '$stateParams', 'Products', 'Categories', 'currencies',
    function($scope, $stateParams, Products, Categories, currencies){
    $scope.productId = $stateParams.productId;
    $scope.rates = currencies.data.rates;
    Products.getProducts().then(function(response){
       var products = response.data.products;
       $scope.product = Products.getProductById(products, $scope.productId);
       Categories.getCategories().then(function(response){
           $scope.category = Categories.getCategoryById(response.data.categories, $scope.product.catid);
       })
    });
}]);