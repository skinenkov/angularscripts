angular.module('EshopApp')
    .controller('mainPageController', ['$scope', 'resolvedProducts', 'Products', 'currencies', function($scope, resolvedProducts, Products, currencies) {
        $scope.products = resolvedProducts.data.products;
        $scope.randomProducts = Products.getRandomProducts($scope.products, 6);
        $scope.rates = currencies.data.rates;
    }]);