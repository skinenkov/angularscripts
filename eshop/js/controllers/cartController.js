angular.module('EshopApp')
    .controller('cartController', ['$scope', 'currencies', 'CartService', 'resolvedProducts', '$timeout', function($scope, currencies, CartService, resolvedProducts, $timeout){
        $scope.rates = currencies.data.rates;
        $scope.products = resolvedProducts.data.products;
        $scope.getItemInfo = function(productId) {
            for(ind in $scope.products) {
                var product = $scope.products[ind];
                if(product['id'].toString() == productId.toString()) {
                    return product;
                }
            }
        }
        $scope.cartData = [];
        $scope.totalCount = 0;
        $scope.collectCart = function() {
            $scope.totalCount = 0;
            $scope.cartData = [];
            $scope.cartItems = CartService.getItems();
            for (ind in $scope.cartItems) {
                var item = $scope.cartItems[ind];
                var info = $scope.getItemInfo(item['id']);
                $scope.cartData.push({
                    'id': item.id,
                    'image': info.image,
                    'name': info.name,
                    'price': item.price,
                    'quan': item.quan,
                    'total': item.price * item.quan
                });
                $scope.totalCount += item.quan;
            }
        };
        $scope.collectCart();
        $scope.showSuccessMessage = false;
        $scope.doBuy = function() {
            $scope.showSuccessMessage = true;
        };
    }]);