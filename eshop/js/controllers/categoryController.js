
angular.module('EshopApp')
    .controller('categoryController', ['$scope', '$stateParams', 'Products', 'Categories', 'currencies',
        function($scope, $stateParams, Products, Categories, currencies){
        $scope.catId = $stateParams.catId;
        $scope.rates = currencies.data.rates;
        Categories.getCategories().then(function(response){
            $scope.category = Categories.getCategoryById(response.data.categories, $scope.catId);
            $scope.childCategories = Categories.getChildCategories(response.data.categories, $scope.catId);
            $scope.childCategories.push($scope.catId);
            Products.getProducts().then(function(response){
                $scope.products = Products.getProductsByCategory(response.data.products, $scope.childCategories);
            });
        });
    }]);